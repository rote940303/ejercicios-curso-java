package ejercicios;

import java.util.Optional;

public class PrincipalGame {
	static String gameName;
	static double playerPosX;
	static double playerPosY;

	interface gameable {
		void startGame();

		default void setGameName(String name) {
			System.out.println(name);
		}
	}

	interface playeable {
		void walk(double x, double y);

		default void setGameName(String name) {
			System.out.println(Optional.ofNullable(name).orElseThrow(() -> {throw new NullPointerException();}));
		}
	}

	interface soundable {
		void playMusic(String song);

		default void setGameName(String name) {
			System.out.println(Optional.ofNullable(name));
		}
	}

	public static void main(String[] args) {
		playeable p1 = (x, y) -> {
			playerPosX = playerPosX + x;
			playerPosY = playerPosY + y;
		};
		p1.walk(1, 1);
		System.out.println("X [" + playerPosX + "] Y [" + playerPosY + "]");
		
		gameable g1 = () -> {
			p1.setGameName(gameName);
		};
		g1.startGame();
		
		soundable s1 = (song) -> {
			if (gameName == null) {
				gameName = song;
			}
		};
		s1.playMusic("Otro Nombre");
		System.out.println(gameName);
	}
	
}
