package ejercicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MasterDAOImpl implements MasterDAO {
	public MasterDAOImpl() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<AlumnosValue> getAlumnos() throws SQLException {
		List<AlumnosValue> lista = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(selectQuery);
				ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				lista.add(new AlumnosValue(rs.getString(ID), rs.getString(NOMBRE), rs.getString(APELLIDOS), rs.getInt(EDAD) + "", rs.getString(EMAIL)));
			}
			return lista;
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public void insertAlumnos(String id, String nombre, String apellidos, String edad, String email) throws SQLException {
		try (Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(insertQuery)) {
			ps.setString(1, id);
			ps.setInt(2, Integer.parseInt(edad));
			ps.setString(3, nombre);
			ps.setString(4, apellidos);
			ps.setString(5, email);
			ps.execute();
		} catch (SQLException e) {
			throw e;
		}
	}
	// UPDATE PRINCIPAL SET AGE = 26, EMAIL = 'rote@praxisglobe.com' WHERE ID = 1;
	@Override
	public boolean actualizaAlumnos(String id, String edad, String email) throws SQLException {
		try (Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(updateQuery)) {
			ps.setInt(1, Integer.parseInt(edad));
			ps.setString(2, email);
			ps.setString(3, id);
			if (ps.executeUpdate() != 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			throw e;
		}
	}
}
