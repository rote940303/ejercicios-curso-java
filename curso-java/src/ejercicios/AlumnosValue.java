package ejercicios;

public class AlumnosValue {

	String nombre;

	String apellidos;

	String edad;

	String ID;

	String email;

	public AlumnosValue(String id, String nombre, String apellidos, String edad, String email) {
		this.ID = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.email = email;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		StringBuffer mensaje = new StringBuffer();
		mensaje.append("ID[" + ID + "]");
		mensaje.append("Nombre[" + nombre + "]");
		mensaje.append("Apellidos[" + apellidos + "]");
		mensaje.append("Edad[" + edad + "]");
		mensaje.append("Email[" + email + "]");
		return mensaje.toString();
	}

}
