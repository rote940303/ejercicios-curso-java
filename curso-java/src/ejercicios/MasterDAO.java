package ejercicios;

import java.sql.SQLException;
import java.util.List;

public interface MasterDAO {
	static final String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	static final String connectionUrl = "jdbc:sqlserver://192.168.56.10:1433;databaseName=CURSO_JAVA;user=enlace;password=enlace";
	static final String user = "enlace";
	static final String user2 = "enlace";
	static final String passwd = "enlace";

	static final String ID = "id";
	static final String EDAD = "age";
	static final String NOMBRE = "first_name";
	static final String APELLIDOS = "last_name";
	static final String EMAIL = "email";

	String selectQuery = "SELECT * FROM PRINCIPAL;";

	String updateQuery = "UPDATE PRINCIPAL SET AGE = ?, EMAIL = ? WHERE ID = ?;";
	
	String insertQuery = "INSERT INTO PRINCIPAL VALUES (?, ?, ?, ?, ?);";

	public List<AlumnosValue> getAlumnos() throws SQLException;

	public void insertAlumnos(String id, String nombre, String apellidos, String edad, String email) throws SQLException;
	
	public boolean actualizaAlumnos(String id, String edad, String email) throws SQLException;
	
}
