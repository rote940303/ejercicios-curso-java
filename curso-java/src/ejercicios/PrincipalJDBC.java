package ejercicios;

import java.sql.SQLException;

public class PrincipalJDBC {

	public static void main(String[] args) {
		try {
			MasterDAO alumnosDAO = AlumnosFactory.getInstance();
			
			alumnosDAO.getAlumnos().forEach(alumnos -> System.out.println(alumnos));
			
			alumnosDAO.insertAlumnos("3", "Jesus", "Brito", "26", "bioj@praxis.com.mx");
			
			if (alumnosDAO.actualizaAlumnos("2","30","rofx@praxis.com.mx")) {
				System.out.println("Registro actualizado correctamente!");
			} else {
				System.err.println("No fue posible actualizar el registro!");
			}
			
			alumnosDAO.getAlumnos().forEach(alumnos -> System.out.println(alumnos));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
