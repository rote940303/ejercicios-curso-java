package ejercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PrincipalAlumnos {

	static List<AlumnosValue> lista = new ArrayList<AlumnosValue>();
	static AlumnosValue alumnos;

	public static void main(String[] args) {

		for (int j = 1; j < 21; j++) {
			alumnos = new AlumnosValue(j + "", "alumno" + j, "apellidos" + j, j + 20 + "", "correo" + j + "@praxis.com.mx");

			lista.add(alumnos);
		}
		System.out.println("###########  1. Imprime el contenido de todos los alumnos de la lista. ########### ");
		System.out.println("");

		lista.stream().forEach(System.out::println);

		System.out.println("");
		System.out.println("###########  2. Imprime la longitud de la lista de alumnos. ########### ");

		System.out.println(lista.stream().count());

		System.out.println("");
		System.out.println("###########  3. Imprime los alumnos cuyo nombre termine con la letrá 'a'. ########### ");

		Predicate<AlumnosValue> p = s -> s.nombre.endsWith("a");
		lista.stream().filter(p).forEach(System.out::println);

		System.out.println("");
		System.out.println("###########  4. Imprime los primeros 5 alumnos de la lista. ########### ");
		lista.stream().limit(5).forEach(System.out::println);

	}
}
